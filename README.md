**iptables-gen**

Use iptables-gen.sh to generate iptables rules for INPUT and FORWARD chains in the filter table. Execute without arguments.
_This script runs ok in Debian GNU/Linux 10.8_

TO DO:
- In interface lo, just ACCEPT all
- Generate rules for others Chains.
