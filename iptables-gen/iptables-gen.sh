#!/bin/bash 
if [ -e /sbin/ip ]; then
	ifaceArray=()
    I1=`ip -o link show | awk -F' ' '{ print $2 }' | awk -F':' '{ print $1 }'`
    for i in $I1; do
    	./iptables-input-gen.sh $i
		ifaceArray+=($i)
    done
	pos=0
	for i in ${ifaceArray[@]}; do
    	lastpos=${#ifaceArray[@]}
    	lastpos=$((lastpos-1))
    	while [ $lastpos -gt $pos ]; do
        	./iptables-forward-gen.sh ${ifaceArray[$pos]} ${ifaceArray[$lastpos]}
        	lastpos=$((lastpos-1))
    	done
    	pos=$((pos+1))
	done
else
    ifconfig | grep flags
fi
