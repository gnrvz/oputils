#!/bin/bash 
IFACE=$1
echo "iptables -t filter -A INPUT -i $IFACE -p tcp -m conntrack --ctstate INVALID -j DROP"
echo "iptables -t filter -A INPUT -i $IFACE -p udp -m conntrack --ctstate INVALID -j DROP"
echo "iptables -t filter -A INPUT -i $IFACE -p tcp -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT"
echo "iptables -t filter -A INPUT -i $IFACE -p udp -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT"
echo "iptables -t filter -A INPUT -i $IFACE -p icmp -j ACCEPT"
echo "iptables -t filter -A INPUT -i $IFACE -p tcp --dport 22 -m conntrack --ctstate NEW -j ACCEPT"
