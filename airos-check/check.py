#!/usr/bin/env python3
import paramiko, ipaddress, socket

CMD='ls'
USR='ubnt'
PSS='ubnt'
PRT=22
NTW='192.168.0.0/24'

wisp = ipaddress.ip_network(NTW)
cpes = wisp.hosts()


def addErrorIp(ip):
    with open('iperror.txt','a') as fn:
        fn.write("{}\n".format(str(ip)))

def isErrorIp(ip):
    ipMatch = False
    try:
        with open('iperror.txt','r') as fn:
            for i in fn:
                if str(ip).strip() == str(i).strip():
                    ipMatch = True
                    break
    except FileNotFoundError:
        with open('iperror.txt','w') as fn:
            pass
        pass
    return ipMatch


client = paramiko.SSHClient()
client.load_system_host_keys()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

for i in cpes:
    if not isErrorIp(str(i)):
        try:
            print("Trying to connect with {}".format(i))
            client.connect(hostname=str(i),port=PRT,username=USR,password=PSS, timeout=2)
            (stdin, stdout, stderr) = client.exec_command(CMD)
            for i in stdout:
                print(i.strip())
        except socket.timeout:
            print("... timeout.")
            addErrorIp(i)
            pass
        except paramiko.ssh_exception.AuthenticationException:
            print("... auth fail")
            addErrorIp(i)
            pass
        except paramiko.ssh_exception.NoValidConnectionsError:
            print("... port error")
            addErrorIp(i)
            pass
        except KeyboardInterrupt:
            print("... exiting.")
            break
        except paramiko.ssh_exception.SSHException:
            print("... not existing session")
            addErrorIp(i)
            pass
